		</div>
		<!-- /.wrapper -->
		<footer id="footer" class="footer">
			<div class="container">
				<div class="footer__wrapp">
					<div class="footer__row">
						<div class="footer__col footer__col_center">
							<a href="tel:8 800 123 45 67" class="footer__phone">
								<span class="s-inline-vertical">
									<?php include  $_SERVER['DOCUMENT_ROOT'].'/images/svg/icons/icon-phone.svg'; ?>
								</span>
								<span class="s-inline-vertical" >8 800 123 45 67</span>
							</a>
							<a href="#" class="footer__mail">
								<span class="s-inline-vertical">
									<?php include  $_SERVER['DOCUMENT_ROOT'].'/images/svg/icons/icon-mail.svg'; ?>

								</span>
								<span class="s-inline-vertical" >info@ravnovesie.ru</span>
							</a>
							<div class="footer__social">
								<ul class="social">
									<li class="s-inline-vertical">
										<a href="#">
											<?php include  $_SERVER['DOCUMENT_ROOT'].'/images/svg/icons/icon-facebook.svg'; ?>
										</a>
									</li>
									<li class="s-inline-vertical footer__social_p2">
										<a href="#">
											<?php include  $_SERVER['DOCUMENT_ROOT'].'/images/svg/icons/icon-vk.svg'; ?>
										</a>
									</li>
									<li class="s-inline-vertical">
										<a href="#">
											<?php include  $_SERVER['DOCUMENT_ROOT'].'/images/svg/icons/icon-insta.svg'; ?>
										</a>
									</li>
								</ul>
								
							</div>
						</div>
						<div class="footer__col footer__col_order2">
							<ul class="footer__list">
								<li>
									<a href="#">О нас</a>
								</li>
								<li>
									<a href="#">Каталог</a>
								</li>
								<li>
									<a href="#" class="footer__price-list">
										<span  class="s-inline-vertical">
											<?php include $_SERVER['DOCUMENT_ROOT'].'/images/svg/icons/icon-price-listl.svg'; ?>
										</span>
										<span class="s-inline-vertical">Прайс</span>
									</a>
								</li>
							</ul>
						</div>
						<div class="footer__col footer__col_order1">
							<ul class="footer__list">
								<li>
									<a href="#">Соц. ответственность</a>
								</li>
								<li>
									<a href="#">Гарантии</a>
								</li>
								<li>
									<a href="#">Техническое задание</a>
								</li>
								<li>
									<a href="#">Доставка</a>
								</li>
								<li>
									<a href="#">Конфеденциальность</a>
								</li>
							</ul>
						</div>
						<div class="footer__col footer__col_mw230">
							<p class="footer__subscribe">
								Подписывайтесь на нашу рассылку
							</p>
							<form action="" class="footer__form">
								<input type="text" class="input" placeholder="Имя">
								<input type="text" class="input" placeholder="Email">
								<buttom class="btn btn_white footer__btn">ПОдписаться</button>
							</form>
							
						</div>
					</div>
				</div>
			</div>
			<div class="footer-bottom">
				<p class="footer-bottom__text">
					2019 © Равновесие. Учебные Модели.рф
				</p>
			</div>
			<!-- /.container -->
		</footer>



		<!-- ============================== -->
		<!-- =========ПОПАП ОКНА=========== -->
		<!-- ============================== -->

		<div class="jsx-modal" data-jsx-modal-id="popup-sign-in">
		    <div class="jsx-modal__block popup-sign_modal-block">
		    	<div class="jsx-modal__close jsx-modal__close_style "></div>
		    	<div class="popup-sign">
		    		<div class="popup-sign__col">
		    			<h3 class="popup-sign__title">Авторизация</h3>
		    			<p class="popup-sign__text">Введите email и пароль, указанные при регистрации</p>
		    			<a href="" class="popup-sign__link jsx-modal__close" data-jsx-modal-target="popup-sign-out">Зерегистрироваться</a>
		    		</div>
		    		<div class="popup-sign__col">
		    			<form action="" class="popup-sign__form">
							<input type="text" class="input input_gray" placeholder="Email">
							<input type="password" class="input input_gray" placeholder="Пароль">
							<!-- Поменяешь на button сделано для проосмотра -->
							<a href="/page-lk-favorite.php" class="btn btn_blue">Войти</a>
							<!-- Поменяешь на button сделано для проосмотра -->
							<label class="checkbox">
				              <input type="checkbox" name="Согласен на обработку?" class="checkbox__checkbox js_checkbox">
				              <div class="checkbox__nesting">
				                <span class="checkbox__square">
				                </span>
				                <p class="checkbox__text s-light-hel equipment-checkbox__text">Я согласен(а) на обработку моих Персональных данных</p>
				              </div>
				            </label>
		    			</form>
		    			<a href="" class="popup-sign__link jsx-modal__close" data-jsx-modal-target="popup-restore-password">Восстановить пароль</a>
		    		</div>
		    	</div>
		    </div>
		  </div>
		  <div class="jsx-modal" data-jsx-modal-id="popup-sign-out">
		    <div class="jsx-modal__block popup-sign_modal-block">
		    	<div class="jsx-modal__close jsx-modal__close_style"></div>
		    	<div class="popup-sign">
		    		<div class="popup-sign__col">
		    			<h3 class="popup-sign__title">Регистрация</h3>
		    			<p class="popup-sign__text">Зарегистрируйтесь, введя свой email и пароль</p>
		    			<a href="" class="popup-sign__link jsx-modal__close" data-jsx-modal-target="popup-sign-in">Авторизоваться</a>
		    		</div>
		    		<div class="popup-sign__col">
		    			<form action="" class="popup-sign__form">
							<input type="text" class="input input_gray" placeholder="Email">
							<input type="password" class="input input_gray" placeholder="Пароль">
							<input type="password" class="input input_gray" placeholder="Повторите Пароль">
							<button class="btn btn_blue popup-sign__btn">Войти</button>
							<label class="checkbox">
				              <input type="checkbox" name="Согласен на обработку?" class="checkbox__checkbox js_checkbox">
				              <div class="checkbox__nesting">
				                <span class="checkbox__square">
				                </span>
				                <p class="checkbox__text s-light-hel equipment-checkbox__text">Я согласен(а) на обработку моих Персональных данных</p>
				              </div>
				            </label>
		    			</form>
		    		</div>
		    	</div>
		    </div>
		  </div>
		   <div class="jsx-modal" data-jsx-modal-id="popup-restore-password">
		    <div class="jsx-modal__block popup-sign_modal-block">
		    	<div class="jsx-modal__close jsx-modal__close_style"></div>
		    	<div class="popup-sign">
		    		<div class="popup-sign__col">
		    			<h3 class="popup-sign__title">Восстановить Пароль</h3>
		    			<p class="popup-sign__text">Введите свой email чтобы восстановить пароль</p>
		    			<a href="" class="popup-sign__link jsx-modal__close" data-jsx-modal-target="popup-sign-in">Авторизоваться</a>
		    		</div>
		    		<div class="popup-sign__col">
		    			<form action="" class="popup-sign__form">
							<input type="text" class="input input_gray" placeholder="Email">
							<button class="btn btn_blue">Восстановить</button>
							<label class="checkbox">
				              <input type="checkbox" name="Согласен на обработку?" class="checkbox__checkbox js_checkbox">
				              <div class="checkbox__nesting">
				                <span class="checkbox__square">
				                </span>
				                <p class="checkbox__text s-light-hel equipment-checkbox__text">Я согласен(а) на обработку моих Персональных данных</p>
				              </div>
				            </label>
		    			</form>
		    		</div>
		    	</div>
		    </div>
		  </div>
		  <div class="jsx-modal" data-jsx-modal-id="popup-outlay">
		    <div class="jsx-modal__block popup-sign_modal-block">
		    	<div class="jsx-modal__close jsx-modal__close_style"></div>
		    	<div class="popup-sign popup-outlay" style="background-image: url(images/background/bg-popup.jpg);">
		    		<div class="popup-sign__col">
		    			<h3 class="popup-sign__title">Получите смету</h3>
		    			<p class="popup-sign__text">Отправьте список необходимых товаров или информацию по тендеру. Наши менеджеры подготовят просчёт в течение 1 часа!</p>
		    		</div>
		    		<div class="popup-sign__col">
		    			<form action="" class="popup-sign__form">
							<input type="text" class="input input_gray" placeholder="Имя">
							<input type="text" class="input input_gray" placeholder="Email">
							<input type="tel" class="input input_gray js-phone" placeholder="Телефон">
							<div class="attach">
								<div class="attach__wrapp-label">
                            		<label class="attach__label"  id="label-file1" for="file1"><input class="left clip-input attach__input" type="file" name="file_name" id="file1">
                            			<span class="attach__icon s-inline-vertical"></span>
                            			<span class="attach__text s-inline-vertical clip-input-txt">Прикрепить документ</span>
                            		</label>
								</div>
                       		</div>
							<button class="btn btn_blue">Получить смету</button>
							<label class="checkbox">
				              <input type="checkbox" name="Согласен на обработку?" class="checkbox__checkbox js_checkbox">
				              <div class="checkbox__nesting">
				                <span class="checkbox__square">
				                </span>
				                <p class="checkbox__text s-light-hel equipment-checkbox__text">Я согласен(а) на обработку моих Персональных данных</p>
				              </div>
				            </label>
		    			</form>
		    		</div>
		    	</div>
		    </div>
		  </div>


		<!-- ============================== -->
		<!-- =========ПОПАП ТОВАРА========= -->
		<!-- ============================== -->

		<div class="jsx-modal" data-jsx-modal-id="popup-product">
		    <div class="jsx-modal__block product-single_modal-block">
		    	<div class="jsx-modal__close jsx-modal__close_style"></div>
		    	<div class="product-single product-single_popup">
		    		<div class="product-single__wrapp">
						<div class="product-single__row">
							<div class="product-single__col">
								<div class="product-single__slider-big">
									<div class="product-single__slide-big" >
										<div class="product-single__box-big">
											<img src="./images/background/bg-header.jpg" alt="">
										</div>
									</div>
									<div class="product-single__slide-big" >
										<div class="product-single__box-big">
											<img src="./images/product/product-single.jpg" alt="">
										</div>
									</div>
									<div class="product-single__slide-big" >
										<div class="product-single__box-big">
											<img src="./images/background/bg-header.jpg" alt="">
										</div>
									</div>
									<div class="product-single__slide-big" >
										<div class="product-single__box-big">
											<img src="./images/product/product-single.jpg" alt="">
										</div>
									</div>
									<div class="product-single__slide-big" >
										<div class="product-single__box-big">
											<img src="./images/background/bg-header.jpg" alt="">
										</div>
									</div>
									
								</div>
								<div class="product-single__slider-small">
									<div class="product-single__slide-small">
										<div class="product-single__box-small">
											<img src="./images/background/bg-header.jpg" alt="">
										</div>
									</div>
									<div class="product-single__slide-small">
										<div class="product-single__box-small">
											<img src="./images/product/product-single.jpg" alt="">
										</div>
									</div>
									<div class="product-single__slide-small">
										<div class="product-single__box-small">
											<img src="./images/background/bg-header.jpg" alt="">
										</div>
									</div>
									<div class="product-single__slide-small">
										<div class="product-single__box-small">
											<img src="./images/product/product-single.jpg" alt="">
										</div>
									</div>
									<div class="product-single__slide-small">
										<div class="product-single__box-small">
											<img src="./images/background/bg-header.jpg" alt="">
										</div>
									</div>
								</div>
							</div>
							<div class="product-single__col">
								<div class="product-single__content">
									<h2 class="product-single__title">
										Учебная карта “»”Европа 
										для средней школы, матовая, ламинирование, 1460*1480мм
									</h2>
								</div>
								<div class="product-single__favorites">
									<p>
										Артикул: 5538
									</p>
									<label class="checkbox">
										<input type="checkbox" name="Согласен на обработку?" class="checkbox__checkbox js_checkbox">
										<div class="checkbox__nesting">
											<span class="checkbox__square">
											</span>
											<p class="checkbox__text">Добавить в избранное</p>
										</div>
									</label>
								</div>
								<p class="product-single__desc">
									Карта такая прекрасная-распрекрасная, очень подробная и детальная, выполнена на 4-х листах общим размером 1460*1480 мм, имеет 2-стороннее матовое ламинирование.
								</p>
								<p class="product-single__text">Рекомендуемое количество на кабинет: 1 шт.</p>
								<div class="product-single__how-much">
									<p class="product-single__text">Сколько Вам?</p>
									<div class="block-number js-number">
										<div class="block-number__minus js-number-minus">-</div>
										<div><input type="number" class="block-number__input js-number-input" value="1"></div>
										<div class="block-number__plus js-number-plus">+</div>
									</div>
								</div>
								<p class="product-single__price">Цена: 1600 р.</p>
								<div class="btn btn_blue">В корзину</div>
								
							</div>
						</div>
					</div>
		    	</div>
		    </div>
		  </div>
		
		<script src="js/jquery-3.3.1.min.js"></script>
		<script src="js/slick.min.js"></script>
		<script src="js/s-slider.js"></script>
		
		<script src="js/s-menu.js"></script>
		
		<script src="js/jquery.accordion.js"></script>
		<script src="js/jquery.cookie.js"></script>

		<script src="js/jquery-ui.js"></script>
		<script src="js/jquery-ui touch punch.js"></script>
		<script src="js/range.js"></script>

		<script src="js/jsx-modal.js"></script>

		<script src="js/jquery.fancybox.min.js"></script>

		<script src="js/input/jquery.inputmask.min.js"></script>
		<script src="js/input/inputmask.phone.extensions.js"></script>
		<script src="js/input/phone.js"></script>
		<script src="js/input/input-mask.js"></script>
		
		<script src="js/main.js"></script>
	</body>
</html>