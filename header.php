<!DOCTYPE html>
<html lang="ru">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<meta name="description" content="">
		<meta name="keywords" content="">
		<link href="favicon.png" rel="shortcut icon" type="image/x-icon" />
		<link rel="stylesheet" href="style.css">
		<title>Название</title>
	</head>
	<body>
		<div id="wrapper" class="wrapper">
			<div class="sidebar-mobile">
				<div class="sidebar-mobile__row">
					<ul class="menu menu_icon-block">
						<li>
							<a href="tel:8 800 123 45 67">
								<span><img src="./images/svg/icons/icon-call.svg" alt=""></span>
								<span>8 800 123 45 67</span>
							</a>
						</li>
					</ul>
					<div class="burger sidebar-mobile__burger js-burger">
						<div class="burger__line"></div>
					</div>
				</div>
				
				<div class="sidebar-mobile__wrapp">
					<ul class="menu">
						<li>
							<a href="/page-catalog.php">Каталог</a>
						</li>
						<li>
							<a href="">Прайс-лист</a>
						</li>
						<li>
							<a href="/page-delivery.php">Доставка</a>
						</li>
						<li>
							<a href="/page-about-company.php">О компании</a>
						</li>
						<li>
							<a href="/page-contacts.php">Контакты</a>
						</li>
						
					</ul>
					<ul class="menu menu_icon-block">
						<li>
							<a href="/page-order-336.php" class="head-top__link-blue">
								<span><img src="./images/svg/icons/icon-list.svg" alt=""></span>
								<span>Приказ 336</span>
							</a>
						</li>
						<li>
							<a href="">
								<span><img src="./images/svg/icons/icon-list-calc.svg" alt=""></span>
								<span>Получить смету</span>
							</a>
						</li>
						<li>
							<a href="">
								<span><img src="./images/svg/icons/icon-help.svg" alt=""></span>
								<span>Онлайн-Вопрос</span>
							</a>
						</li>
						<li>
							<a href="">
								<span><img src="./images/svg/icons/icon-group.svg" alt=""></span>
								<span>Мы на Портале поставщиков</span>
							</a>
						</li>
						
					</ul>
				</div>
				<ul class="menu menu_icon-block">
					<li>
						<a href="" data-jsx-modal-target="popup-sign-in">
							<span><img src="./images/svg/icons/icon-exit.svg" alt=""></span>
							<span>Вход | Регистрация</span>
						</a>
					</li>
				</ul>
				
			</div>
			<div class="head js-head">
				<div class="head-top">
					<div class="container">
						<div class="head-top__wrapp">
							<ul class="head-top__list">
								<li>
									<a href="tel:8 800 123 45 67">
										<span><img src="./images/svg/icons/icon-call.svg" alt=""></span>
										<span>8 800 123 45 67</span>
									</a>
								</li>
								<li>
									<a href="">
										<span><img src="./images/svg/icons/icon-help.svg" alt=""></span>
										<span>Онлайн-Вопрос</span>
									</a>
								</li>
								<li>
									<a href="">
										<span><img src="./images/svg/icons/icon-group.svg" alt=""></span>
										<span>Мы на Портале поставщиков</span>
									</a>
								</li>
							</ul>
							<ul class="head-top__list">
								<li>
									<a href="/page-order-336.php" class="head-top__link-blue">
										<span><img src="./images/svg/icons/icon-list.svg" alt=""></span>
										<span>Приказ 336</span>
									</a>
								</li>
								<li>
									<a href=""  data-jsx-modal-target="popup-outlay">
										<span><img src="./images/svg/icons/icon-list-calc.svg" alt=""></span>
										<span>Получить смету</span>
									</a>
								</li>
								<li>
									<a href="" data-jsx-modal-target="popup-sign-in">
										<span><img src="./images/svg/icons/icon-exit.svg" alt=""></span>
										<span>Вход | Регистрация</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="head-bottom">
					<div class="container">
						<div class="head-bottom__wrapp">
							<div class="head-bottom__item">
								<ul class="menu head-bottom__menu-desktop">
									<!-- Этот php код сделан для визуальной наглядности ты можешь его удалить -->
									<li>
										<a href="/page-catalog.php" class="<?php if($_SERVER['SCRIPT_NAME'] == '/page-catalog.php') echo 'active' ?>">Каталог</a>
									</li>
									<li>
										<a href="">Прайс-лист</a>
									</li>
									<li>
										<a href="/page-delivery.php" class="<?php if($_SERVER['SCRIPT_NAME'] == '/page-delivery.php') echo 'active' ?>">Доставка</a> 
									</li>
									<li>
										<a href="/page-about-company.php" class="<?php if($_SERVER['SCRIPT_NAME'] == '/page-about-company.php') echo 'active' ?>">О компании</a>
									</li>
									<li>
										<a href="/page-contacts.php" class="<?php if($_SERVER['SCRIPT_NAME'] == '/page-contacts.php') echo 'active' ?>">Контакты</a>
									</li>
								</ul>
								<div class="burger js-burger ">
									<div class="burger__line"></div>
								</div>
								<input type="text" class="input-search head-bottom__input-search head-bottom__input-search_mobile" placeholder="Поиск по сайту">
								<div class="head-bottom__search-cover"></div>
							</div>
							<div class="logo">
								<a href="/">
									<img src="./images/logo.png" alt="">
								</a>
							</div>
							<div class="head-bottom__item">
								<form class="head-bottom__row">
									<input type="text" class="input-search head-bottom__input-search head-bottom__input-search_desktop" placeholder="Поиск по сайту">
									<a href="/page-basket.php" class="block-link-shopping-cart">
										<span>
											<?php include  $_SERVER['DOCUMENT_ROOT'].'/images/svg/icons/icon-shopping-cart.svg'; ?>
										</span>
										<span>0.00</span>
										<span>
											<?php include  $_SERVER['DOCUMENT_ROOT'].'/images/svg/icons/icon-ruble.svg'; ?>
										</span>
									</a>
									<div class="head-bottom__search-cover"></div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>