<?php include 'header.php'; ?>
<header class="header js-slider-header">
	<?php for ($i=0; $i < 5; $i++) { ?>
		<div class="header__slide" style="background-image: url(./images/background/<?= $i % 2 ? 'bg-how-we-work.jpg' : 'bg-header.jpg'?> );">
			<div class="container">
				<div class="header__wrapp">
					<div class="header__content">
						<h1 class="header__title">Знания - путь к свободе</h1>
						<h2 class="header__sub-title">Средства обучения для учебных заведений</h2>
						<a href="" class="header__link">Подробнее</a>
					</div>
				</div>
			</div>	
			<!-- /.container -->
		</div>
	<?php } ?>
</header>
<section class="how-we-work"  style="background-image: url(./images/background/bg-how-we-work.jpg);">
	<div class="spheres">
		<div class="container">
			<div class="spheres__slider js-spheres-slider">
				<div class="spheres__slide">
					<a href="" class="spheres__block-link">
						<div class="spheres__image">
							<img src="images/svg/lessons/icon-science.svg">
						</div>
						<div class="spheres__name">
							Физика
						</div>
					</a>
				</div>
				<div class="spheres__slide">
					<a href="" class="spheres__block-link">
						<div class="spheres__image">
							<img src="images/svg/lessons/icon-science.svg">
						</div>
						<div class="spheres__name">
							Физика
						</div>
					</a>
				</div>
				<div class="spheres__slide">
					<a href="" class="spheres__block-link">
						<div class="spheres__image">
							<img src="images/svg/lessons/icon-science.svg">
						</div>
						<div class="spheres__name">
							Физика
						</div>
					</a>
				</div>
				<div class="spheres__slide">
					<a href="" class="spheres__block-link">
						<div class="spheres__image">
							<img src="images/svg/lessons/icon-science.svg">
						</div>
						<div class="spheres__name">
							Физика
						</div>
					</a>
				</div>
				<div class="spheres__slide">
					<a href="" class="spheres__block-link">
						<div class="spheres__image">
							<img src="images/svg/lessons/icon-science.svg">
						</div>
						<div class="spheres__name">
							Физика
						</div>
					</a>
				</div>	
			</div>
		</div>
	</div>

	<div class="container">
		<div class="how-we-work__wrapp">
			<h3 class="sub-title">Как мы работаем</h3>
			<div class="how-we-work__block-way">
				<div class="how-we-work__row ">
					<div class="how-we-work__col">
						<div class="how-we-work__block">
							<div class="how-we-work__images">
								<img src="./images/svg/elements/icon-create-order.svg" alt="" class="how-we-work__img">
							</div>
							<div class="how-we-work__info">
								<p class="how-we-work__name">Сформируйте заказ</p>
								<p class="how-we-work__text s-light-hel">выберите необходимые товары и положите их в корзину</p>
							</div>
						</div>
					</div>
					<div class="how-we-work__col how-we-work__col_position how-we-work__col_position_left">
						<div class="how-we-work__block how-we-work__block_arrow">
							<div class="how-we-work__images">
								<img src="./images/svg/elements/icon-issue-order.svg" alt="" class="how-we-work__img">
							</div>
							<div class="how-we-work__info">
								<p class="how-we-work__name">Оформите заявку</p>
								<p class="how-we-work__text s-light-hel">отправьте заявку через корзину, указав контактную информацию и адрес доставки</p>
							</div>
						</div>
					</div>
					<div class="how-we-work__col">
						<div class="how-we-work__block ">
							<div class="how-we-work__images">
								<img src="./images/svg/elements/icon-pay-order.svg" alt="" class="how-we-work__img">
							</div>
							<div class="how-we-work__info">
								<p class="how-we-work__name">Оплатите заказ</p>
								<p class="how-we-work__text s-light-hel">расчет производиться безналичным платежом</p>
							</div>
						</div>
					</div>
					<div class="how-we-work__col how-we-work__col_position how-we-work__col_position_right">
						<div class="how-we-work__block how-we-work__block_arrow">
							<div class="how-we-work__images">
								<img src="./images/svg/elements/icon-delivery.svg" alt="" class="how-we-work__img">
							</div>
							<div class="how-we-work__info">
								<p class="how-we-work__name">Доставка</p>
								<p class="how-we-work__text s-light-hel">мы работаем с лучшими логистами, которые доставят Ваш заказ в течение 7 дней</p>
							</div>
						</div>
					</div>
					<div class="how-we-work__col">
						<div class="how-we-work__block">
							<div class="how-we-work__images">
								<img src="./images/svg/elements/icon-evolution.svg" alt="" class="how-we-work__img">
							</div>
							<div class="how-we-work__info">
								<p class="how-we-work__name">Долгосрочное развитие</p>
								<p class="how-we-work__text s-light-hel">скидки для постоянных клиентов и бонусные программы</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</div>
</section>
<section class="goods">
	<div class="container">
		<div class="goods__wrapp">
			<h3 class="sub-title">Лидеры продаж</h3>
			<div class="goods__row slider js-slider">
				<?php for ($i=0; $i < 10; $i++) { ?>
					<div class="goods__col">
						<div class="block-product">
							<div class="block-product__image">
								<img src="./images/calculator.png" alt="" class="block-product__img">
							</div>
							<div class="block-product__info">
								<p class="block-product__name">Инженерный калькулятор</p>
								<p class="block-product__model s-light-hel">модель: Д320</p>
								<p class="block-product__price s-light-hel">990.-</p>
								<div class="btn btn_gray block-product__btn">В корзину</div>
							</div>
							<a href="/page-product-single.php" class="block-product__link"></a>
						</div>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
</section>
<section class="reviews">
	<div class="container">
		<div class="reviews__wrapp">
			<h3 class="sub-title">Отзывы клиентов</h3>
			<div class="reviews__row slider js-slider">
				<?php for ($i=0; $i < 10; $i++) { ?>
					<div class="reviews__col">
						<div class="block-review">
							<div class="block-review__image">
								<img src="./images/reviews.png" alt="" class="block-review__img">
							</div>
							<div class="block-review__info">
								<p class="block-review__text s-light-hel">
									«Не знаю что обычно пишут в отзывах. Но в этом случае удержаться не могу и напишу как получится. Компанию нам посоветовал хороший знакомый. Его рекомендации были абсолютно оправданы. Огромное спасибо за работу!»
								</p>
								<p class="block-review__person">Петр Фортепьянов, администратор СШ №168, г. Москва</p>
							</div>
						</div>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
</section>
<?php include 'footer.php'; ?>