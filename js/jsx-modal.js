﻿$(function() {
	var jsxModalClick = $('[data-jsx-modal-target]');
	var jsxModal 	  = $('.jsx-modal');
	var jsxModalClose = $('.jsx-modal__close');

	var jsxBody = $('body');
	var $this;
	// Open popup 
	jsxModalClick.click(function(event) {
		event.preventDefault();
		$(this).addClass('jsx-active');
		$this = $(this);
		var dataJsxModal = $(this).data('jsx-modal-target');
		$("[data-jsx-modal-id='" + dataJsxModal + "']").fadeIn();
		jsxBody.addClass('jsx-hidden');

	});

	// close popup
	jsxBody.click(function(e) {
		var jsxTargetBlock = $(e.target);
		var clickModal;
		// alert(jsxTargetBlock.closest('[data-jsx-modal-target]').length);
		//jsxTargetBlock.has('[data-jsx-modal-target]').length || 
		if(jsxTargetBlock.closest('[data-jsx-modal-target]').length) {
			clickModal = true;
		}
		if(jsxModalClick.hasClass('jsx-active')) {
			if(jsxTargetBlock.closest('.jsx-modal__block').length == 0 && !clickModal) {
				console.log ('close tel__me');
				jsxModal.fadeOut();
				jsxBody.removeClass('jsx-hidden');
			}
		}
	});

	jsxModalClose.click(function(event) {
		var jsxModal = $(this).closest('.jsx-modal');
		jsxModal.fadeOut();
		jsxBody.removeClass('jsx-hidden');
	});

});