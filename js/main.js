﻿(function ($) {
  
  /// Плавающее меню
  $(window).on('scroll load', function(event) {
    event.preventDefault();
    $('.js-head').toggleClass('active', $(this).scrollTop() > 1);
  });


  

 
  // Добавления цифр в инпут
  (function quantityProducts() {
    var $quantityArrowMinus = $(".js-number-minus");
    var $quantityArrowPlus = $(".js-number-plus");

    var $quantityNum;

    $quantityArrowMinus.click(function(){
      quantityMinus(this);
    });
    $quantityArrowPlus.click(function(){
      quantityPlus(this);
    });

    function quantityMinus($this) {
      $quantityNum = $($this).closest('.js-number').find('.js-number-input');
      if ($quantityNum.val() > 1) {
        $quantityNum.val(+$quantityNum.val() - 1);
      }
    }

    function quantityPlus($this) {
      $quantityNum = $($this).closest('.js-number').find('.js-number-input');
      $quantityNum.val(+$quantityNum.val() + 1);
    }
	})();



   //Акордеон
	$('.accordion').dcAccordion({
		speed: 300,
	});
  // Аккордеон для фильров
  $('.accordion-filter').dcAccordion({
    speed: 300,
    autoClose: false,
  });



	// Подробнее таблица Товаров
   $(document).on('click load', '.js-open-product-more', function(event) {
   	var parent = $(this).closest('.tr');
   	if ($(this).find('.checkbox__checkbox').is(':checked')) {
   		parent.addClass('active-product');
   	}
   	else{
   		parent.removeClass('active-product');
   	}
   });

   // STYLE TABLE AND PLAT
   var clickTabStyle = $('.js-tab-style');
   var contentTabStyle = $('.js-table-product');
   $(document).on('click', '.js-tab-style', function(event) {
     event.preventDefault();
     clickTabStyle.removeClass('active');
     $(this).addClass('active');
     if ($(this).hasClass('active-table')) {
       contentTabStyle.removeClass('active');
     }
     if (!$(this).hasClass('active-table')) {
        contentTabStyle.addClass('active');
     }
   });


   // TABS
   $(".js-tab-parent .js-tab").click(function() {
		console.log('click to .tab');
	    $(this).closest('.js-tab-parent').find(".js-tab").removeClass("active");
	  	$(this).addClass("active");
	    $(this).closest('.js-tab-parent').find(".js-tab-content").hide().eq($(this).index()).fadeIn()
	}).eq(0).addClass("active");


   // Мой Аккордеон
   if(window.matchMedia('(max-width: 991px)').matches)
    {
      $('.js-accordeon-parent .js-accordeon-custom').click(function(event) {
        event.preventDefault();
        $(this).parents('.js-accordeon-parent').find('.js-accordeon-custom').next().not(this).slideUp().prev().removeClass('active');
        $(this).next().not(':visible').slideDown().prev().addClass('active');
      });
    }



    // Редактирование данных
    var editBlock = $('.js-setting-list');
    $('.js-btn-edit').on('click',  function(event) {
      event.preventDefault();
      $(this).toggleClass('btn_green');
      if ($(this).hasClass('btn_green')) {
       editBlock.addClass('active-edit');
       $(this).text('Сохранить');
      }
      if (!$(this).hasClass('btn_green')) {
       editBlock.removeClass('active-edit');
       $(this).text('Редактировать');
      }
      
    });

    // Редактирование Пароля
    $('.js-btn-edit-pass').on('click',  function(event) {
      event.preventDefault();
      $(this).hide();
      $('.my-setting__password').fadeIn();
    });

    





    // ГАЛЕРЕЯ
    if ($('[data-fancybox]').length >= 1) {
      $('[data-fancybox]').fancybox({
        // thumbs : {
        //   autoStart : bols
        // },
        buttons: [
            // "zoom",
            //"share",
            // "slideShow",
            //"fullScreen",
            //"download",
            "thumbs",
            "close"
          ],
        youtube : {
              controls : 0,
              showinfo : 1
          },
      });
    }
      


    // Attach file
    $(".clip-input").change(function() {
        var filename = $(this).val().replace(/.*\\/, "");
        $(".clip-input-txt").text(filename);
    }); 




     var product = [];
    //ДОБАВЛЕНИЕ ТОВАРА В LOCAL STORAGE
    var checkboxFavorites = $('.js-add-favorites-checkbox');
    checkboxFavorites.on('change', function(event) {
       event.preventDefault();
       var $this = $(this);
      
       if ($this.is(':checked')) {
        product.push($this.data('product'));
        // ДОБАВЛЯЕМ В СТОРАЖЕ
        localStorage.setItem("product", JSON.stringify(product));
       }
       else{
        // РЕСЕТИМ ПЕРВЫЙ МАССИВ
        product = product.filter(function(elem) {
            // console.log(elem);
             return elem !== Number($this.attr('data-product'));
        });
        // РЕСЕТИМ МАССИВ С ЛОКАЛ СТОРАЖЕ
        var productGet = JSON.parse(localStorage.getItem("product"));
        productGet = productGet.filter(function(elem) {
            // console.log(elem);
             return elem !== Number($this.attr('data-product'));
        });
        // ОБНОВЛЯЕМ В СТОРАЖЕ
        localStorage.setItem("product",JSON.stringify(productGet));
       }
      








      
    });


})(jQuery);
