﻿
$(document).ready(function() {
	if ($('.js-range').length >= 1) {
    var jsRange = $('.js-range');
    var jsMinVal = $('.range-value-1');
    var jsMaxVal = $('.range-value-2');

    var vals = jsRange.attr('data-value').split(',');

    var jsMinValInput = $('.range-min');
    var jsMaxValInput = $('.range-max');
    // console.log(vals);
    jsRange.slider({
        range: true,
        min: Number(jsRange.attr('data-range-min')),
        max: Number(jsRange.attr('data-range-max')),
        values: [ vals[0], vals[1] ],
        slide: function( event, ui ) {
          jsMinVal.text( ui.values[ 0 ] );
          jsMaxVal.text( ui.values[ 1 ] );        

          jsMinValInput.val( ui.values[ 0 ] );
          jsMaxValInput.val( ui.values[ 1 ] );
        },
        classes: {
        "ui-slider": "range-slider"
      }
      });
      jsMinVal.text(jsRange.slider( "values", 0 ));
      jsMaxVal.text(jsRange.slider( "values", 1 )); 

      jsMinValInput.val(jsRange.slider( "values", 0 ));
      jsMaxValInput.val(jsRange.slider( "values", 1 ));

  }

});