﻿(function ($) {
  	var btn = $('.js-burger');
  	var body = $('body');
 	btn.on('click', function(event) {
	  	event.preventDefault();
	  	var documentClose = $(this).closest(document);
	  	btn.toggleClass('active');
	  	if($(this).hasClass('active')) {
			body.addClass('js_active-sidebar');
			// close
			sidebarClose(this,documentClose,'js_active-sidebar','sidebar-mobile');
		}
		if(!$(this).hasClass('active')) {
			body.removeClass('js_active-sidebar')
			documentClose.unbind('click.myEvent'); 
		}
  	});
	function sidebarClose($this,$docum,$class,$closest) {
		var firstClick = true;
		$docum.bind('click.myEvent', function(e) {
			console.log($(e.target).closest('.'+$closest+'').length);
			if (!firstClick && $(e.target).closest('.'+$closest+'').length == 0) {
				btn.removeClass('active');
				body.removeClass($class);
				$docum.unbind('click.myEvent');
			}
			
			firstClick = false;
		});

	}


})(jQuery);
