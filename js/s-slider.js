﻿(function ($) {
  if ($('.js-spheres-slider').length >= 1) {
    $('.js-spheres-slider').slick({
       arrows:true,
       dots:false,
       slidesToShow: 5,
       slidesToScroll: 1,
       nextArrow: '<div class="slider-arrow slider-arrow_blue slider-arrow_right" aria-hidden="true"></div>',
       prevArrow: '<div class="slider-arrow slider-arrow_blue slider-arrow_left" aria-hidden="true"></div>',
       responsive: [
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3
          }
        },
        {
          breakpoint: 730,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
        
      ]
    });
  }



  if ($('.js-equipment-slider').length >= 1) {
    var $sliderPrimary = $('.js-equipment-spheres-slider');
    $sliderPrimary.slick({
       arrows:true,
       dots:false,
       slidesToShow: 5,
       slidesToScroll: 1,
       focusOnSelect: true,
       nextArrow: '<div class="slider-arrow slider-arrow_blue slider-arrow_right" aria-hidden="true"></div>',
       prevArrow: '<div class="slider-arrow slider-arrow_blue slider-arrow_left" aria-hidden="true"></div>',
       asNavFor: '.js-equipment-slider',
       responsive: [
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3
          }
        },
        {
          breakpoint: 730,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
        
      ]
    });
    $('.js-equipment-slider').slick({
       arrows:false,
       dots:false,
       slidesToShow: 1,
       slidesToScroll: 1,
       asNavFor: '.js-equipment-spheres-slider'
    });

    $sliderPrimary.on('afterChange', function(event, slick, currentSlide){
      $('.js-get-price').text($(slick.$slides.get(currentSlide)).attr('data-price'));
      $('.js-name-equipment').val($(slick.$slides.get(currentSlide)).attr('data-name'));
    });   

  

  }

    


  if ($('.js-slider').length >= 1) {
    $('.js-slider').slick({
       arrows:true,
       dots:false,
       slidesToShow: 4,
       slidesToScroll: 1,
       nextArrow: '<div class="slider-arrow slider-arrow_right" aria-hidden="true"></div>',
       prevArrow: '<div class="slider-arrow slider-arrow_left" aria-hidden="true"></div>',
       responsive: [
        {
          breakpoint: 1199,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3
          }
        },
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        },
        {
          breakpoint: 730,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
        
      ]
    });
  }

  // СЛАЙДЕР ПРОДУКТА ДВА
  if ($('.product-single__slider-big').length >= 1) {
    var bigSlider = $('.product-single__slider-big');
    var smallSlider = $('.product-single__slider-small');
    function getSliderSettings(){
        return {
          arrows:false,
          slidesToShow: 1,
          slidesToScroll: 1,
          focusOnSelect: true,
          fade: true,
          cssEase: 'linear',
          infinite: true,
          adaptiveHeight: true,
          asNavFor: '.product-single__slider-small'
      }
    }
    function getSliderSettingsTwo(){
        return {
          arrows:true,
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: true,
          nextArrow: '<div class="slider-arrow slider-arrow_right" aria-hidden="true"></div>',
          prevArrow: '<div class="slider-arrow slider-arrow_left" aria-hidden="true"></div>',
          asNavFor: '.product-single__slider-big'
      }
    }

      bigSlider.slick(getSliderSettings());
      smallSlider.slick(getSliderSettingsTwo()); 

    $('[data-jsx-modal-target="popup-product"]').on('click', function(event) {
     bigSlider.slick('unslick');
     smallSlider.slick('unslick');

    bigSlider.not('.slick-initialized').slick(getSliderSettings());
    smallSlider.not('.slick-initialized').slick(getSliderSettingsTwo());
    });
  }



if ($('.js-slider-header').length >= 1) {
    $('.js-slider-header').slick({
        arrows:true,
        dots:false,
        slidesToShow: 1,
        slidesToScroll: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        nextArrow: '<div class="slider-arrow slider-arrow_blue slider-arrow_right" aria-hidden="true"></div>',
        prevArrow: '<div class="slider-arrow slider-arrow_blue slider-arrow_left" aria-hidden="true"></div>',
    });
}





})(jQuery);
