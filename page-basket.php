<?php include 'header.php'; ?>

<section class="basket">
	<div class="container">
		<ul class="bread-crumbs bread-crumbs_p2">
			<li>
				<a href="#">Главная </a>
			</li>
			<li>
				<a href="#">Корзина</a>
			</li>
		</ul>
		<h3 class="basket__title">
			Корзина
		</h3>
		<div class="basket__table-block">
			<table class="basket__table">
				<thead>
					<tr>
						<th>Код</th>
						<th>Категория</th>
						<th>Товар</th>
						<th>Количество</th>
						<th>Стоимость Р.</th>
						<th>Сумма Р.</th>
					</tr>
				</thead>
				<tbody>
					<?php for ($i=0; $i < 15; $i++) { ?>
					<tr>
						<td>1616</td>
						<td>География</td>
						<td>Учебная карта Австралии

							<ul class="basket__table-list-mobile">
								<li><span>Код :</span>  32323</li>
								<li><span>Категория :</span>  География</li>
								<li><span>Стоимость Р. :</span>   1500</li>
								<li><span>Сумма Р. :</span> 3000</li>
								
							</ul>
						</td>
						<td>
							<div class="basket__table-number-delete">
								<div class="basket__table-flex">
									<div class="block-number block-number_transparent js-number">
										<div class="block-number__minus js-number-minus">-</div>
										<div><input type="number" class="block-number__input js-number-input" value="1"></div>
										<div class="block-number__plus js-number-plus">+</div>
									</div>
									<div class="basket__delete" title="Удалить">
										<img src="./images/svg/icons/icon-delete.svg" alt="">
									</div>
								</div>
							</div>
							
						</td>
						<td>1500</td>
						<td>3000</td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
		<ul class="info-list">
			<li class="info-list__item">
				<p class="info-list__item-text">
					Стоимость товаров
				</p>
				<p class="info-list__item-text s-light-hel">
					55380,00
				</p>
			</li>
			<li class="info-list__item">
				<p class="info-list__item-text">
					Накопительная скидка
				</p>
				<p class="info-list__item-text s-light-hel">
					5%
				</p>
			</li>
			<li class="info-list__item">
				<p class="info-list__item-text">
					Сезонная распродажа:
				</p>
				<p class="info-list__item-text s-light-hel">
					30%
				</p>
			</li>
			<li class="info-list__item">
				<p class="info-list__item-text">
					Стоимость со скидкой
				</p>
				<p class="info-list__item-text s-light-hel">
					33180,00
				</p>
			</li>
		</ul>
		<button class="btn btn_blue basket__btn">Оформить заказ</button>
	</div>
</section>

<?php include 'footer.php'; ?>