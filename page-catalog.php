<?php include 'header.php'; ?>

<section class="main">
	<div class="container">
		<ul class="bread-crumbs main__breads-crumbs">
			<li>
				<a href="#">Главная </a>
			</li>
			<li>
				<a href="#">Каталог</a>
			</li>
			<li>
				<a href="#">карта материков</a>
			</li>
		</ul>
		<div class="main__wrapp">
			<div class="sidebar js-accordeon-parent">
				<h3 class="sidebar__title sidebar__title_mobile js-accordeon-custom">
					Кабинеты
				</h3>
				<ul class="accordion sidebar__list">
					<li>
					    <a href="">
					       Астрономия
					    </a>
				        <ul>
				            <li>
							    <a href="">
							       Литература
							    </a>
						        <ul>
						            <li><a href="">Технические средства обучения</a></li>
						            <li><a href="">Технические средства обучения</a></li>
						            <li><a href="">Технические средства обучения</a></li>
						        </ul>
							</li>
				            <li><a href="">Технические средства обучения</a></li>
				            <li><a href="">Технические средства обучения</a></li>
				        </ul>
					</li>
					<li>
					    <a href="">
					       Биология
					    </a>
				        <ul>
				            <li><a href="">Технические средства обучения</a></li>
				            <li><a href="">Технические средства обучения</a></li>
				            <li><a href="">Технические средства обучения</a></li>
				        </ul>
					</li>
					<li>
					    <a href="">
					       География
					    </a>
				        <ul>
				            <li><a href="">Технические средства обучения</a></li>
				            <li><a href="">Технические средства обучения</a></li>
				            <li><a href="">Технические средства обучения</a></li>
				        </ul>
					</li>
					<li>
					    <a href="">
					       История
					    </a>
				        <ul>
				            <li><a href="">Технические средства обучения</a></li>
				            <li><a href="">Технические средства обучения</a></li>
				            <li><a href="">Технические средства обучения</a></li>
				        </ul>
					</li>
					<li>
					    <a href="">
					       Литература
					    </a>
				        <ul>
				            <li><a href="">Технические средства обучения</a></li>
				            <li><a href="">Технические средства обучения</a></li>
				            <li><a href="">Технические средства обучения</a></li>
				        </ul>
					</li>
					
				</ul>

				<h3 class="sidebar__title sidebar__title_mobile js-accordeon-custom">
					Фильтры
				</h3>
				<ul class="accordion-filter sidebar__list">
					<li>
					    <a href="">
					       Цена
					    </a>
				        <ul>
				            <li>
				            	<div class="range">
				            		 <div class="range-text d-flex d-f-between">
		                                 <p><span class="range-value-1" >1</span>руб.</p>
		                                 <p><span class="range-value-2" >10000</span> руб.</p>
		                                 <input type="hidden" class="range-min">
		                                 <input type="hidden" class="range-max">
		                            </div>
		                            <div class="js-range" data-range-min="1" data-range-max="10000" data-value="1000,6000"></div>
				            	</div>
	                           
				            </li>
				        </ul>
					</li>
					<li>
					    <a href="">
					       Размер 
					    </a>
				        <ul class="sidebar__size-list">
				            <li>
				            	<label class="checkbox checkbox_check">
									<input type="checkbox" name="" class="checkbox__checkbox js_checkbox">
									<div class="checkbox__nesting">
										<span class="checkbox__square">
										</span>
										<p class="checkbox__text">размер 1</p>
									</div>
								</label>
				            </li>
				             <li>
				            	<label class="checkbox checkbox_check">
									<input type="checkbox" name="" class="checkbox__checkbox js_checkbox">
									<div class="checkbox__nesting">
										<span class="checkbox__square">
										</span>
										<p class="checkbox__text">размер 1</p>
									</div>
								</label>
				            </li>
				             <li>
				            	<label class="checkbox checkbox_check">
									<input type="checkbox" name="" class="checkbox__checkbox js_checkbox">
									<div class="checkbox__nesting">
										<span class="checkbox__square">
										</span>
										<p class="checkbox__text">размер 1</p>
									</div>
								</label>
				            </li>
				        </ul>
					</li>
					<li>
					    <a href="">
					       Тип
					    </a>
				        <ul class="sidebar__size-list">
				             <li>
				            	<label class="checkbox checkbox_radio">
									<input type="radio" name="111" class="checkbox__checkbox js_checkbox">
									<div class="checkbox__nesting">
										<span class="checkbox__square">
										</span>
										<p class="checkbox__text">тип 1</p>
									</div>
								</label>
				            </li>
				             <li>
				            	<label class="checkbox checkbox_radio">
									<input type="radio" name="111" class="checkbox__checkbox js_checkbox">
									<div class="checkbox__nesting">
										<span class="checkbox__square">
										</span>
										<p class="checkbox__text">тип 1</p>
									</div>
								</label>
				            </li>
				             <li>
				            	<label class="checkbox checkbox_radio">
									<input type="radio" name="111" class="checkbox__checkbox js_checkbox">
									<div class="checkbox__nesting">
										<span class="checkbox__square">
										</span>
										<p class="checkbox__text">тип 1</p>
									</div>
								</label>
				            </li>
				        </ul>
					</li>
					
					
				</ul>
			</div>
			<ul class="main__tabs-product">
				<li class="js-tab-style active-table active">
					<?php include $_SERVER['DOCUMENT_ROOT']."/images/svg/icons/icon-table-style.svg"; ?>
				</li>
				<li class="js-tab-style">
					<?php include $_SERVER['DOCUMENT_ROOT']."/images/svg/icons/icon-block-style.svg"; ?>
				</li>
			</ul>
			<div class="content">
				<div class="content__item  content__item_product-table">
					<div class="table-product js-table-product">
						<ul class="table-product__list">
							<li class="th">Код</li>
							<li class="th table-product_th_big">Товар</li>
							<li class="th">В корзину 
								<span class="table-product_th-icon">
									<?php include $_SERVER['DOCUMENT_ROOT']."/images/svg/icons/icon-shopping-cart.svg"; ?>
								</span>
							</li>
							<li class="th">Рек. кол-во</li>
							<li class="th">Сумма Р.
								<span class="table-product_th-icon">
									<?php include $_SERVER['DOCUMENT_ROOT']."/images/svg/icons/icon-ruble.svg"; ?>
								</span>
							</li>
						</ul>
						<h3 class="content__title table-product__title">Карты материков</h3>
						<div class="table-product__row">
							<?php for ($i=0; $i < 9; $i++) { ?>
								<div class="table-product__col">
									<!-- Табличный -->
									<ul class="table-product__list tr">
										<li class="td">1616</li>
										<li class="td">
											<div class="table-product__product" data-jsx-modal-target="popup-product">
												<div class="table-product__product-image">
													<a href="">
														<img class="s-cover-img" src="./images/product/product-single.jpg" alt="">
													</a>
												</div>
												<div class="table-product__product-text">
													Учебная карта Европа для средней школы, матовая, ламинирование, 1460*1480мм
												</div>
											</div>
											<div class="table-product__mobile">
												<p class="table-product__mobile-code">Код : 32323</p>
												<div class="block-number js-number">
													<div class="block-number__minus js-number-minus">-</div>
													<div><input type="number" class="block-number__input js-number-input" value="1"></div>
													<div class="block-number__plus js-number-plus">+</div>
												</div>
											</div>
										</li>
										<li class="td">
											<label class="checkbox js-open-product-more">
												<input type="checkbox" class="checkbox__checkbox js_checkbox">
												<div class="checkbox__nesting">
													<span class="checkbox__square">
													</span>
												</div>
											</label>
										</li>
										<li class="td">
											<div class="block-number block-number_transparent js-number">
												<div class="block-number__minus js-number-minus">-</div>
												<div><input type="number" class="block-number__input js-number-input" value="1"></div>
												<div class="block-number__plus js-number-plus">+</div>
											</div>
										</li>
										<li class="td">1500</li>
									</ul>
									<!-- Блочный -->
									<div class="block-product">
										<div class="block-product__image">
											<img src="./images/calculator.png" alt="" class="block-product__img">
										</div>
										<div class="block-product__info">
											<p class="block-product__name">Инженерный калькулятор</p>
											<p class="block-product__model s-light-hel">модель: Д320</p>
											<p class="block-product__price s-light-hel">990.-</p>
											<div class="btn btn_gray block-product__btn">В корзину</div>
										</div>
										<a href="" class="block-product__link"></a>
									</div>
								</div>
							<?php } ?>
						</div>
						<a href="" class="content__more">Показать еще</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php for ($i=0; $i < 9; $i++) { ?>
	<label class="checkbox js-open-product-more">
		<input type="checkbox" class="checkbox__checkbox js-add-favorites-checkbox" data-product="<?= $i ?>">
		<div class="checkbox__nesting">
			<span class="checkbox__square">
			</span>
		</div>
	</label>
<?php } ?>

<?php include 'footer.php'; ?>