<?php include 'header.php'; ?>
<section class="contacts s-padding-top-160px">
	<div class="container">
		<div class="contacts__wrapp">
			<h3 class="contacts__title">Контакты</h3>
			<div class="contacts__row">
				<div class="contacts__col contacts__col_pb">
					<ul class="contacts__list">
						<li class="contacts__item">
							<p class="contacts__name">АДРЕС</p>
							<p class="contacts__text">Москва, Красная пл., дом 1</p>
						</li>
						<li class="contacts__item">
							<p class="contacts__name">ТЕЛЕФОН</p>
							<p class="contacts__text">+7 (495) 001-22-33</p>
						</li>
						<li class="contacts__item">
							<p class="contacts__name">Email</p>
							<p class="contacts__text">info@ravnovesie.ru</p>
						</li>
						<li class="contacts__item">
							<p class="contacts__name">режим работы</p>
							<p class="contacts__text">пн-пт с 9:00 до 18:00</p>
						</li>
					</ul>
					<div class="contacts__social">
						<ul class="social">
							<li class="s-inline-vertical">
								<a href="#">
									<?php include  $_SERVER['DOCUMENT_ROOT'].'/images/svg/icons/icon-facebook.svg'; ?>
								</a>
							</li>
							<li class="s-inline-vertical footer__social_p2">
								<a href="#">
									<?php include  $_SERVER['DOCUMENT_ROOT'].'/images/svg/icons/icon-vk.svg'; ?>
								</a>
							</li>
							<li class="s-inline-vertical">
								<a href="#">
									<?php include  $_SERVER['DOCUMENT_ROOT'].'/images/svg/icons/icon-insta.svg'; ?>
								</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="contacts__col">
					<div class="map">
						<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A028066fc452ab111c9890b6f4c27831dc9affdd8c2eea74dfd93615d723c4e40&amp;width=100%&amp;height=362&amp;lang=ru_RU&amp;scroll=false"></script>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php include 'footer.php'; ?>