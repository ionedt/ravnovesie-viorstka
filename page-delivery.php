<?php include 'header.php'; ?>
<section class="delivery s-padding-top-160px">
	<div class="container">
		<ul class="bread-crumbs">
			<li>
				<a href="#">Главная </a>
			</li>
			<li>
				<a href="#">Доставка</a>
			</li>
			<li>
				<a href="#">Доставка</a>
			</li>
			<li>
				<a href="#">Доставка</a>
			</li>
		</ul>
		<div class="delivery__wrapp">
			<div class="delivery__content">
				<h3 class="text-title">Доставка</h3>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
				</p>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
				</p>
				<ol>
					<li>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
					</li>
					<li>
						Do eiusmod tempor incididunt ut labore et dolore magna aliqua.
					</li>
					<li>
						Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris
					</li>
					<li>
						Nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor.
					</li>
				</ol>
				<ul>
					<li>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
					</li>
					<li>
						Do eiusmod tempor incididunt ut labore et dolore magna aliqua.
					</li>
					<li>
						Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris
					</li>
					<li>
						Nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor.
					</li>
				</ul>
				<blockquote>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
					</p>
				</blockquote>
				<h4>Lorem ipsum</h4>
				<p>
					Dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
				</p>
				<p>
					Dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
				</p>
			</div>
		</div>
	</div>

</section>
<?php include 'footer.php'; ?>