<?php include 'header.php'; ?>

<section class="main">

	<div class="container">
		<ul class="bread-crumbs main__breads-crumbs">
			<li>
				<a href="/">Главная </a>
			</li>
			<li>
				<a href="#">Личный Кабинет</a>
			</li>
			<li>
				<a href="#">Изобранное</a>
			</li>
		</ul>
		<div class="main__wrapp">
			<div class="sidebar">
				<h3 class="sidebar__title sidebar__title_padding_0">
					Личный кабинет
				</h3>
				<ul class="sidebar__lk-list">
					<li>
						<a href="/page-lk-favorite.php" class="active">Избранное</a>
					</li>
					<li>
						<a href="/page-lk-history.php">История покупок</a>
					</li>
					<li>
						<a href="/page-lk-settings.php">Мои настройки</a>
					</li>
				</ul>
			</div>
			<div class="content content_lk_favorite"> 
				<section class="goods goods_col-3">
					<div class="goods__row">
						<?php for ($i=0; $i < 9; $i++) { ?>
							<div class="goods__col">
								<div class="block-product">
									<div class="block-product__image">
										<img src="./images/calculator.png" alt="" class="block-product__img">
									</div>
									<div class="block-product__info">
										<p class="block-product__name">Инженерный калькулятор</p>
										<p class="block-product__model s-light-hel">модель: Д320</p>
										<p class="block-product__price s-light-hel">990.-</p>
										<div class="btn btn_gray block-product__btn">В корзину</div>
									</div>
									<a href="" class="block-product__link"></a>
								</div>
							</div>
						<?php } ?>
					</div>
				</section>
				<div class="t-align-center"><a href="" class="content__more">Показать еще</a></div>
			</div>
		</div>
	</div>
</section>

<?php include 'footer.php'; ?>