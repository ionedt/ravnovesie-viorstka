<?php include 'header.php'; ?>

<section class="main">

	<div class="container">
		<ul class="bread-crumbs main__breads-crumbs">
			<li>
				<a href="#">Главная </a>
			</li>
			<li>
				<a href="#">Личный Кабинет</a>
			</li>
			<li>
				<a href="#">Изобранное</a>
			</li>
		</ul>
		<div class="main__wrapp">
			<div class="sidebar">
				<h3 class="sidebar__title sidebar__title_padding_0">
					Личный кабинет
				</h3>
				<ul class="sidebar__lk-list">
					<li>
						<a href="/page-lk-favorite.php" class="">Избранное</a>
					</li>
					<li>
						<a href="/page-lk-history.php" class="active">История покупок</a>
					</li>
					<li>
						<a href="/page-lk-settings.php">Мои настройки</a>
					</li>
				</ul>
			</div>
			<div class="content">
				<div class="history">
					<div class="history__row">
						<div class="history__col">
							<ul class="history__head history__tr-one">
								<div>Дата</div>
								<div>№ заказа</div>
								<div>Кол-во <br> товаров</div> 
								<div>Сумма</div>
								<div>Статус</div>
							</ul>
							<ul class=" accordion history__accordion">
								<?php for ($i=0; $i < 5; $i++) { ?>
									<li>
										<a href="" class="history__link-one ">
											<div class="history__tr-one">
												<div>23.12.15</div>
												<div>123124 <?php echo $i; ?></div>
												<div>2</div>
												<div>1500</div>
												<div>выполнен</div>
											</div>
										</a>
										<ul class="history__list-two">
											<li class="history__mobile-data">
												<div><span>Дата:</span>23.07.19</div>
												<div><span>Статус:</span>Выполнено</div>
											</li>
											<?php for ($j=0; $j < 3; $j++) { ?>
											<li>
												<a href="" class="history__link-two ">
													<div class="history__tr-two">
														<div><span>К-т Географии</span></div>
														<div>32</div>
														<div>7890</div>
													</div>
												</a>
												<ul class="history__list-three">
													<li>Карта материков крупных территорий</li>
													<li>Учебная карта "Австралия и Новая Зеландия" (физическая) </li>
													<li>Учебная карта "Австралия и Новая Зеландия" (физическая) </li>
													<li>Учебная карта "Австралия и Новая Зеландия" (физическая) </li>
													<li>Учебная карта "Австралия и Новая Зеландия" (физическая) </li>
													<li>Учебная карта "Австралия и Новая Зеландия" (физическая) </li>
													<li>Учебная карта "Австралия и Новая Зеландия" (физическая) </li>
													<li>Учебная карта "Австралия и Новая Зеландия" (физическая) </li>
												</ul>
											</li>
											<?php } ?>
										</ul>
									</li>
								<?php } ?>
							</ul>
						</div>
						<div class="history__col">
							<div class="history__info">
								<h3 class="history__order-title">Ваш заказ №5896</h3>
								<ul class="info-list">
									<li class="info-list__item">
										<p class="info-list__item-text">
											Почтовая
										</p>
										<p class="info-list__item-text s-light-hel">
											04.23.2015
										</p>
									</li>
									<li class="info-list__item">
										<p class="info-list__item-text">
											Накопительная скидка
										</p>
										<p class="info-list__item-text s-light-hel">
											04.23.2015
										</p>
									</li>
									<li class="info-list__item">
										<p class="info-list__item-text">
											Накопительная скидка
										</p>
										<p class="info-list__item-text s-light-hel">
											04.23.2015
										</p>
									</li>
									<li class="info-list__item">
										<p class="info-list__item-text">
											Сезонная распродажа:
										</p>
										<p class="info-list__item-text s-light-hel">
											04.23.2015
										</p>
									</li>
									<li class="info-list__item">
										<p class="info-list__item-text">
											Стоимость со скидкой
										</p>
										<p class="info-list__item-text s-light-hel">
											04.23.2015
										</p>
									</li>
								</ul>
								<p class="history__status">Статус: <span>доставка</span></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php include 'footer.php'; ?>