<?php include 'header.php'; ?>

<section class="main">

	<div class="container">
		<ul class="bread-crumbs main__breads-crumbs">
			<li>
				<a href="#">Главная </a>
			</li>
			<li>
				<a href="#">Личный Кабинет</a>
			</li>
			<li>
				<a href="#">Мои Настройки</a>
			</li>
		</ul>
		<div class="main__wrapp">
			<div class="sidebar">
				<h3 class="sidebar__title sidebar__title_padding_0">
					Личный кабинет
				</h3>
				<ul class="sidebar__lk-list">
					<li>
						<a href="/page-lk-favorite.php" class="">Избранное</a>
					</li>
					<li>
						<a href="/page-lk-history.php" >История покупок</a>
					</li>
					<li>
						<a href="/page-lk-settings.php" class="active">Мои настройки</a>
					</li>
				</ul>
			</div>
			<div class="content content_lk_setting">
				<div class="my-setting">
					<div class="my-setting__row">
						<div class="my-setting__col">
							<h3 class="my-setting__title">
								Мои настройки
							</h3>
							<ul class="my-setting__list js-setting-list">
								<li class="my-setting__item">
									<p class="my-setting__label">ФИО</p>
									<input type="text" class="input input_line" placeholder="Имя" value="Алексей Богдановия">
								</li>
								<li class="my-setting__item">
									<p class="my-setting__label">Компания</p>
									<input type="text" class="input input_line" placeholder="Компания" value="Название такое то">
								</li>
								<li class="my-setting__item">
									<p class="my-setting__label my-setting__label_ttn">Номер телефона</p>
									<input type="tel" class="input input_line js-phone" placeholder="Телефона" value="380961032749">
								</li>
								<li class="my-setting__item">
									<p class="my-setting__label my-setting__label_ttn">E-mail</p>
									<input type="text" class="input input_line" placeholder="E-mail" value="awdwa@gmail.com">
								</li>
								<li class="my-setting__item">
									<p class="my-setting__label">Адрес доставки</p>
									<input type="text" class="input input_line" placeholder="Адрес" value="Улица 2. Стоительная">
								</li>
							</ul>
							<div class="my-setting__buttons">
								<button class="btn btn_gray my-setting__btn my-setting__btn_mr20 s-d-inl-vam js-btn-edit">Редактировать</button>
								<button class="btn btn_blue my-setting__btn  s-d-inl-vam js-btn-edit-pass">Изменить пароль</button>
							</div>
							<div class="my-setting__password " style="display: none;">
								<ul class="my-setting__list active-edit">
									<li class="my-setting__item">
										<p class="my-setting__label">ТЕКУЩИЙ ПАРОЛЬ</p>
										<input type="password" class="input input_line" placeholder="">
									</li>
									<li class="my-setting__item">
										<p class="my-setting__label">НОВЫЙ ПАРОЛЬ</p>
										<input type="password" class="input input_line" placeholder="">
									</li>
									<li class="my-setting__item">
										<p class="my-setting__label my-setting__label_ttn">ПОВТОРИТЕ НОВЫЙ ПАРОЛЬ</p>
										<input type="password" class="input input_line" placeholder="">
									</li>
								</ul>
								<button class="btn btn_blue my-setting__btn my-setting__btn_mt10 s-d-inl-vam">Изменить пароль</button>
							</div>
						</div>
						<div class="my-setting__col">
							<div class="my-setting__image">
								<a href="#">
									<img class="s-cover-img" src="./images/add.png" alt="">
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php include 'footer.php'; ?>