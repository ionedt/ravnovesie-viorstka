<?php include 'header.php'; ?>
<header class="header-decree" style="background-image: url(images/background/bg-decree-top.jpg);">
	
</header>
<section class="decree" >
	<div class="container">
		<ul class="bread-crumbs bread-crumbs_p2">
			<li>
				<a href="#">Главная </a>
			</li>
			<li>
				<a href="#">Приказ 336</a>
			</li>
		</ul>
		<div class="decree__wrapp">
			<div class="decree__content">
				<div class="decree__info">
					<h3 class="text-title">Поставки согласно приказу 336</h3>
					<p class="decree__text s-light-hel">
						Комплектуем “под ключ” школы-новостройки, по приказу Мин. образования и науки РФ от 30 марта 2016 г. № 336
					</p>
				</div>
				<blockquote>
					<p>
						Приказ № 336 от 30.03.2016 года "Об утверждении перечня средств обучения и воспитания, необходимых для реализации образовательных программ начального общего, основного общего и среднего общего образования, соответствующих современным условиям обучения, необходимого при оснащении общеобразовательных организаций...
					</p>
					<a href="#">Текст приказа</a>
				</blockquote>
			</div>
		</div>
	</div>
</section>
<section class="positions">
	<div class="container">
		<div class="positions__wrapp">
			<div class="positions__title">
				<p>В нашем каталоге есть все позиции, необходимые для <br>комплектации школы, согласно приказу 336</p>
			</div>
			<div class="spheres">
				<div class="container">
					<div class="spheres__slider js-equipment-spheres-slider">
						<?php for ($i=0; $i < 8; $i++) { ?> 
							<div class="spheres__slide" data-price="<?php echo $i* 100; ?>" data-name="Тут будет нормас назва<?php echo $i; ?>">
								<div class="spheres__block-link">
									<div class="spheres__image">
										<img src="images/svg/lessons/icon-science.svg">
									</div>
									<div class="spheres__name">
										Физика
									</div>
								</div>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<p class="dwadawd"></p>
<section class="equipment">
	<div class="container">
		<div class="equipment__wrapp">
			<div class="equipment__row">
				<div class="equipment__col equipment__col_list">
					<div class="js-equipment-slider">
						<?php for ($i=1; $i < 8; $i++) { ?>
							<div class="js-equipment-slide" >
								<p class="equipment__title s-light-hel">
									Комплектация кабинета:
								</p>
								<table class="equipment__table">
									<tr>
										<td>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed</p>
										</td>
										<td>2 шт. </td>
									</tr>
									<tr>
										<td>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed</p>
										</td>
										<td>2 шт. </td>
									</tr>
									<tr>
										<td>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed</p>
										</td>
										<td>2 шт. </td>
									</tr>
									<tr>
										<td>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed</p>
										</td>
										<td>2 шт. </td>
									</tr>
								</table>
							</div>
						<?php } ?>
					</div>
					
				</div>
				<div class="equipment__col equipment__col_pl70">
					<p class="equipment__name s-bold-hel">
						Стоимость одного кабинета
					</p>
					<p class="equipment__price s-light-hel">от <span class="js-get-price">100</span> руб.</p>
					<form action="" class="equipment__form">
						<input type="text" class="input input_gray js-name-equipment" placeholder="тут будет имя оно скроеться">
						<input type="text" class="input input_gray" placeholder="Имя">
						<input type="text" class="input input_gray" placeholder="Телефон">
						<input type="tel" class="input input_gray js-phone" placeholder="Email">
						<button class="btn btn_blue equipment__btn">Получить смету</button>
						<label class="checkbox">
			              <input type="checkbox" name="Согласен на обработку?" class="checkbox__checkbox js_checkbox">
			              <div class="checkbox__nesting">
			                <span class="checkbox__square">
			                </span>
			                <p class="checkbox__text s-light-hel equipment-checkbox__text">Я согласен(а) на обработку моих Персональных данных</p>
			              </div>
			            </label>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="schools">
	<div class="container">
		<div class="schools__wrapp">
			<p class="schools__title s-bold-hel">Школы, которые мы оснастили согласно приказу 336</p>
			<div class="schools__row">
				<?php for ($i=0; $i < 6 ; $i++) { ?>
					
					<div class="schools__col">
						<div class="schools__image">
							<a href="./images/school.png" data-fancybox="schools"><img class="s-cover-img" src="./images/school.png" alt=""></a>
						</div>
						<div class="schools__info">
							<p class="schools__name">Школа № 7</p>
							<p class="schools__city">г. Тверь</p>
						</div>
					</div>

				<?php } ?>
				
			</div>
		</div>
	</div>
</section>
<section class="advice" style="background-image: url(images/background/bg-advice.jpg);">
	<div class="container">
		<div class="advice__wrapp">
			<div class="advice__row">
				<div class="advice__col">
					<div class="advice__info">
						<p class="advice__title">
							Получите консультацию специалиста
						</p>
						<p class="advice__text s-light-hel">
							Оставьте свои контакты и перезвоним чтобы ответить на <span>все вопросы</span>, помочь с <span>выбором товара</span> и <span>поддготовим смету</span> в течение 1 часа
						</p>
					</div>
				</div>
				<div class="advice__col">
					<div class="advice__record">
						<form action="" class="advice__form">
							<input type="text" class="input input_gray" placeholder="Имя">
							<input type="tel" class="input input_gray js-phone" placeholder="Телефон">
							<input type="text" class="input input_gray" placeholder="Email">
							<button class="btn btn_blue advice__btn">ПРоконсультироваться</button>
							<label class="checkbox">
				              <input type="checkbox" name="Согласен на обработку?" class="checkbox__checkbox js_checkbox">
				              <div class="checkbox__nesting">
				                <span class="checkbox__square">
				                </span>
				                <p class="checkbox__text s-light-hel equipment-checkbox__text">Я согласен(а) на обработку моих Персональных данных</p>
				              </div>
				            </label>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php include 'footer.php'; ?>
