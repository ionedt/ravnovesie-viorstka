<?php include 'header.php'; ?>

<section class="product-single product-single_padding">
	<div class="container">
		<div class="product-single__wrapp">
			<div class="product-single__row">
				<div class="product-single__col">
					<div class="product-single__slider-big">
						<div class="product-single__slide-big" >
							<div class="product-single__box-big">
								<img src="./images/background/bg-header.jpg" alt="">
							</div>
						</div>
						<div class="product-single__slide-big" >
							<div class="product-single__box-big">
								<img src="./images/product/product-single.jpg" alt="">
							</div>
						</div>
						<div class="product-single__slide-big" >
							<div class="product-single__box-big">
								<img src="./images/background/bg-header.jpg" alt="">
							</div>
						</div>
						<div class="product-single__slide-big" >
							<div class="product-single__box-big">
								<img src="./images/product/product-single.jpg" alt="">
							</div>
						</div>
						<div class="product-single__slide-big" >
							<div class="product-single__box-big">
								<img src="./images/background/bg-header.jpg" alt="">
							</div>
						</div>
						
					</div>
					<div class="product-single__slider-small">
						<div class="product-single__slide-small">
							<div class="product-single__box-small">
								<img src="./images/background/bg-header.jpg" alt="">
							</div>
						</div>
						<div class="product-single__slide-small">
							<div class="product-single__box-small">
								<img src="./images/product/product-single.jpg" alt="">
							</div>
						</div>
						<div class="product-single__slide-small">
							<div class="product-single__box-small">
								<img src="./images/background/bg-header.jpg" alt="">
							</div>
						</div>
						<div class="product-single__slide-small">
							<div class="product-single__box-small">
								<img src="./images/product/product-single.jpg" alt="">
							</div>
						</div>
						<div class="product-single__slide-small">
							<div class="product-single__box-small">
								<img src="./images/background/bg-header.jpg" alt="">
							</div>
						</div>
					</div>
				</div>
				<div class="product-single__col">
					<div class="product-single__content">
						<h2 class="product-single__title">
							Учебная карта “»”Европа 
							для средней школы, матовая, ламинирование, 1460*1480мм
						</h2>
					</div>
					<div class="product-single__favorites">
						<p>
							Артикул: 5538
						</p>
						<label class="checkbox">
							<input type="checkbox" name="Согласен на обработку?" class="checkbox__checkbox js_checkbox">
							<div class="checkbox__nesting">
								<span class="checkbox__square">
								</span>
								<p class="checkbox__text">Добавить в избранное</p>
							</div>
						</label>
					</div>
					<p class="product-single__desc">
						Карта такая прекрасная-распрекрасная, очень подробная и детальная, выполнена на 4-х листах общим размером 1460*1480 мм, имеет 2-стороннее матовое ламинирование.
					</p>
					<p class="product-single__text">Рекомендуемое количество на кабинет: 1 шт.</p>
					<div class="product-single__how-much">
						<p class="product-single__text">Сколько Вам?</p>
						<div class="block-number js-number">
							<div class="block-number__minus js-number-minus">-</div>
							<div><input type="number" class="block-number__input js-number-input" value="1"></div>
							<div class="block-number__plus js-number-plus">+</div>
						</div>
					</div>
					<p class="product-single__price">Цена: 1600 р.</p>
					<div class="btn btn_blue">В корзину</div>
					
				</div>
			</div>
		</div>
	</div>
</section>
<section class="goods">
	<div class="container">
		<div class="goods__wrapp">
			<h3 class="sub-title">С этим товаром также покупают</h3>
			<div class="goods__row slider js-slider">
				<?php for ($i=0; $i < 10; $i++) { ?>
					<div class="goods__col">
						<div class="block-product">
							<div class="block-product__image">
								<img src="./images/calculator.png" alt="" class="block-product__img">
							</div>
							<div class="block-product__info">
								<p class="block-product__name">Инженерный калькулятор</p>
								<p class="block-product__model s-light-hel">модель: Д320</p>
								<p class="block-product__price s-light-hel">990.-</p>
								<div class="btn btn_gray block-product__btn">В корзину</div>
							</div>
							<a href="" class="block-product__link"></a>
						</div>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
</section>
<section class="goods">
	<div class="container">
		<div class="goods__wrapp">
			<h3 class="sub-title">Лидеры продаж</h3>
			<div class="goods__row slider js-slider">
				<?php for ($i=0; $i < 10; $i++) { ?>
					<div class="goods__col">
						<div class="block-product">
							<div class="block-product__image">
								<img src="./images/calculator.png" alt="" class="block-product__img">
							</div>
							<div class="block-product__info">
								<p class="block-product__name">Инженерный калькулятор</p>
								<p class="block-product__model s-light-hel">модель: Д320</p>
								<p class="block-product__price s-light-hel">990.-</p>
								<div class="btn btn_gray block-product__btn">В корзину</div>
							</div>
							<a href="" class="block-product__link"></a>
						</div>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
</section>



<?php include 'footer.php'; ?>